setwd("D:/Ram/Sales/files machine learn")
#question 1)
churn <- read.csv('churn.csv')

churn.prepared <- churn
str(churn.prepared)
summary(churn.prepared)
?substr
churn.prepared$id_part1 <- as.factor(substr(churn.prepared$customerID, 1, 4))
churn.prepared$id_part2 <- as.factor(substr(churn.prepared$customerID, 6, 10))

#so id useless
churn.prepared$id_part1 <- NULL
churn.prepared$id_part2 <- NULL
churn.prepared$customerID <- NULL
?factor
summary(churn.prepared$Churn)
churn.prepared$SeniorCitizen <- factor(churn.prepared$SeniorCitizen, levels = c(0,1), labels = c('no', 'yes'))
str(churn.prepared$SeniorCitizen)
churn.prepared$tenure <- as.numeric(churn$tenure)
#question 2
library('ggplot2')


ggplot(churn.prepared, aes(Churn,tenure)) + geom_boxplot()
ggplot(churn.prepared, aes(tenure, fill = Churn)) + geom_histogram()
ggplot(churn.prepared, aes(tenure, fill = Churn)) + geom_histogram(position = 'fill')

str(as.factor(churn.prepared$Churn[churn.prepared$tenure == 0]))

ggplot(churn.prepared, aes(Churn)) + geom_bar()
ggplot(churn.prepared, aes(tenure)) + geom_bar(aes(fill = Churn))
ggplot(churn.prepared) +aes(x = tenure, fill = Churn) + geom_bar(position = "fill")

#gender effect chrun
ggplot(churn.prepared, aes(gender)) + geom_bar(aes(fill = Churn))
ggplot(churn.prepared) +aes(x = gender, fill = Churn) + geom_bar(position = "fill")
#answer no'

churn.prepared$Contract <- as.factor(churn.prepared$Contract)
ggplot(churn.prepared, aes(Contract)) + geom_bar(aes(fill = Churn))
ggplot(churn.prepared) +aes(x = Contract, fill = Churn) + geom_bar(position = "fill")

?table
conf_matrix_billing <- table(churn.prepared$PaperlessBilling , churn.prepared$Churn)
#positive- customer stay
# i say he will stay / actual stay
recall <- conf_matrix_billing[1,1]/(conf_matrix_billing[1,1] + conf_matrix_billing[1,2])
# i say he will stay / predict stay - 
#if ill say yes alwas recall will be 100 
presicion <- conf_matrix_billing[1,1]/(conf_matrix_billing[1,1] + conf_matrix_billing[2,1])

install.packages('rpart')
library(rpart)
install.packages('rpart.plot')
library(rpart.plot)

#question 3
library(caTools)
filter <- sample.split(churn.prepared$gender, SplitRatio = 0.7)
churn.prepared.train <- subset(churn.prepared, filter == T)
churn.prepared.test <- subset(churn.prepared, filter == F)


model.dt <- rpart(Churn ~ ., churn.prepared.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "gray", nn = TRUE)

prediction <- predict(model.dt,churn.prepared.test)
actual <- churn.prepared.test$Churn

#cf <- table(actual,prediction > 0.5)
cf <- table(actual,prediction[,'Yes'] > 0.5)
tree.precision <- cf['No','FALSE']/(cf['No','FALSE'] + cf['No','TRUE'])
tree.recall <- cf['No','FALSE']/(cf['No','FALSE'] + cf['Yes','FALSE'])
accurcy <- (cf[1,1] + cf[2,2]) / (cf[1,1] +cf[1,2] + cf[2,1]+cf[2,2])
table(churn.prepared$Churn)
shahih <- length(churn.prepared$Churn[churn.prepared$Churn == 'No']) /length( churn.prepared$Churn)
change <- accurcy - shahih
#really bad :(

loan.model <- glm(as.factor(Churn) ~ ., family = binomial(link = "logit"), data = churn.prepared.train)

summary(loan.model)

pridected.loan.test <- predict(loan.model, newdata = churn.prepared.test, type = 'response')

#confusuion matrix 

confusion_matrix <- table(churn.prepared.test$Churn,pridected.loan.test>0.5)

recall.glm <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[2,1])
precision.glm <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[1,2])



install.packages('pROC')
library(pROC)

rocCurveTree <- roc(actual,prediction[,'Yes'], direction = ">", levels = c("Yes", "No"))
rocCurveRF <- roc(actual,pridected.loan.test, direction = ">", levels = c("Yes", "No"))

#plot the chart 
plot(rocCurveTree, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveTree)
auc(rocCurveRF)

tree <- cf
reg <- confusion_matrix
#matay sheani omer yazov aval hu lo
ifelse(tree[1,2] > reg[1,2], 'reg better', 'tree better')
